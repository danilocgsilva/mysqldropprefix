#!/bin/bash

## version
VERSION="1.0.0"

## Main function
mysqldropprefix () {
  set -e
  local cred_or_lpath
  asks_cred_or_lpath
  if [ $cred_or_lpath = credential ]; then
    dropwithcredential
  elif [ $cred_or_lpath = loginpath ]; then
    dropwithloginpath
  fi
}

## In case of user chooses the "credential" mode, then proceed here
dropwithcredential () {
  read -p "Provides the database username: " username
  read -s -p "Provides the database password: " password
  echo ""
  read -p "Provides the database name: " database_name
  read -p "Provides the prefix value: " prefix
  read -p "Provides a path to backup prefixed table (I wont skip this important security process!): " prefixed_backup
  backup_path=$prefixed_backup/$database_name-$prefix-$(date +%Y%m%d_%Hh%Mm%Ss)
  mkdir -p $backup_path
  for i in $(mysql -u$username -p$password $database_name -e "SHOW TABLES" | grep -iE "^$prefix")
  do
    echo Backuping $i to $backup_path
    mysqldump -u$username -p$password $database_name $i > $backup_path/$i.sql
    echo Droping $i from $database_name
    mysql -u$username -p$password $database_name -e "SET foreign_key_checks = 0; DROP TABLE $i"
  done
  echo $backup_path
}

## In case of user chooses the "loginpath" mode, then proceed here
dropwithloginpath () {
  read -p "Provides the login-path value: " login_path
  read -p "Provides the database name: " database_name
  read -p "Provides the prefix value: " prefix
  read -p "Provides a path to backup prefixed table (I wont skip this important security process!): " prefixed_backup
  backup_path=$prefixed_backup/$database_name-$prefix-$(date +%Y%m%d_%Hh%Mm%Ss)
  mkdir -p $backup_path
  for i in $(mysql --login-path=$login_path $database_name -e "SHOW TABLES" | grep -iE "^$prefix")
  do
    echo Backuping $i to $backup_path
    mysqldump --login-path=$login_path $database_name $i > $backup_path/$i.sql
    echo Droping $i from $database_name
    mysql --login-path=$login_path $database_name -e "SET foreign_key_checks = 0; DROP TABLE $i"
  done
  echo $backup_path
}

## Asks to the user if he or she wants the type of mode
asks_cred_or_lpath () {
  read -p "You will provides the credentials or the login path? Type \"credential\" or \"loginpath\": " cred_or_lpath

  if [[ ! $cred_or_lpath =~ (credential|loginpath) ]]; then
    ask_loop
  fi
}

## In case of the user not providing the right answer, falls in a more direct question loop.
ask_loop () {
  read -p "Type \"credential\" or \"loginpath\": " cred_or_lpath
  if [[ ! $cred_or_lpath =~ (credential|loginpath) ]]; then
    ask_loop
  fi
}

## detect if being sourced and
## export if so else execute
## main function with args
if [[ /usr/local/bin/shellutil != /usr/local/bin/shellutil ]]; then
  export -f mysqldropprefix
else
  mysqldropprefix "${@}"
  exit 0
fi
